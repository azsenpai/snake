
from const import *
from coord import *

from random import randint

class Food:
    def __init__(self):
        self.__alive = False

    def __str__(self):
        s = FOOD
        s += EOL

        if hasattr(self, '_Food__coord'):
            s += str(self.__coord)

        return s

    def __eq__(self, right):
        if isinstance(right, Coord):
            if hasattr(self, '_Food__coord'):
                return self.__coord == right
            return False

        return False

    def isalive(self):
        return self.__alive

    def die(self):
        self.__alive = False

    def create(self, snakes):
        self.__alive = True

        while True:
            self.__coord = Coord(randint(1, N-2), randint(1, M-2))
            end = True

            for snake in snakes:
                if snake.gets(self.__coord) != None:
                    end = False
                    break

            if end: break

    def gets(self, coord):
        if self == coord: return FOOD
