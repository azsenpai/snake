#!/usr/bin/env python

from const import *

from time import sleep
from os import system

from platform import system as system_name

from msvcrt import kbhit
from msvcrt import getch

from coord import Coord
from snake import Snake

from food import Food

def setcolor():
    if system_name().upper() == 'WINDOWS':
        system('color B')
    else:
        system('setterm -foreground blue')

def cls():
    if system_name().upper() == 'WINDOWS':
        system('cls')
    else:
        system('clear')

def draw():
    cls()
    output = ''

    for i in range(N):
        for j in range(M):
            s = None

            if i == 0:
                if j == 0: s = WALL_1
                elif j == M-1: s = WALL_3
                else: s = WALL_H
            elif i == N-1:
                if j == 0: s = WALL_2
                elif j == M-1: s = WALL_4
                else: s = WALL_H
            else:
                if j == 0 or j == M-1:
                    s = WALL_V
                else:
                    coord = Coord(i, j)

                    s = snake.gets(coord)

                    if s == None: s = food.gets(coord)
                    if s == None: s = EMPTY
            output += s
        output += EOL

    output += EOL + 'Snake length: ' + str(len(snake))
    output += EOL + 'Please press ESC to exit '

    print(output)
    print()

    sleep(0.03)

if __name__ == '__main__':
    snake = Snake(3, 5)
    food = Food()

    setcolor()

    while snake.isalive():
        if kbhit():
            key = ord(getch())

            if key == KEY_ESC: break
            elif key == 224: key = ord(getch())

            if key == KEY_UP or key == KEY_W:
                snake.setdir(DIR_U)
            elif key == KEY_DOWN or key == KEY_S:
                snake.setdir(DIR_D)
            elif key == KEY_RIGHT or key == KEY_D:
                snake.setdir(DIR_R)
            elif key == KEY_LEFT or key == KEY_A:
                snake.setdir(DIR_L)

        if not food.isalive(): food.create([snake])
        snake.move([food])

        draw()

    print('written by: zh.adlet@gmail.com')
    input('Press Enter to exit')
