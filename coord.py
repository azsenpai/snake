
class Coord:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return '(%d, %d)' % (self.x, self.y)

    def __add__(self, right):
        return Coord(self.x + right.x, self.y + right.y)

    def __sub__(self, right):
        return Coord(self.x - right.x, self.y - right.y)

    def __eq__(self, right):
        return self.x == right.x and self.y == right.y

    def getdir(self, right):
        from const import DELTA

        from const import DIR_R
        from const import DIR_L
        from const import DIR_U
        from const import DIR_D

        from const import DIRS

        for DIR in DIRS:
            if self + DELTA[DIR] == right: return DIR

        if self + DELTA[DIR_U] + DELTA[DIR_R] == right: return DIR_U
        if self + DELTA[DIR_U] + DELTA[DIR_L] == right: return DIR_U

        if self + DELTA[DIR_D] + DELTA[DIR_R] == right: return DIR_D
        if self + DELTA[DIR_D] + DELTA[DIR_L] == right: return DIR_D

        if self + DELTA[DIR_R] + DELTA[DIR_U] == right: return DIR_R
        if self + DELTA[DIR_R] + DELTA[DIR_D] == right: return DIR_R

        if self + DELTA[DIR_L] + DELTA[DIR_U] == right: return DIR_L
        if self + DELTA[DIR_L] + DELTA[DIR_D] == right: return DIR_L
