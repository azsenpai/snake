
from const import *
from coord import *

class Snake:
    def __init__(self, x, l):
        if l < 3: raise Exception

        self.__alive = True

        self.__coords = []
        for y in range(l, 0, -1): self += Coord(x, y)

        self.setdir(DIR_R)

    def __str__(self):
        s = self.__DIR
        s += EOL

        for p in self.__coords:
            s += str(p)
            s += EOL

        return s

    def __add__(self, right):
        if isinstance(right, Coord):
            self.__coords.append(right)
        return self

    def __len__(self):
        return len(self.__coords)

    def setdir(self, DIR):
        if not hasattr(self, '_Snake__DIR') \
            or self.__DIR == DIR_R and DIR != DIR_L \
            or self.__DIR == DIR_L and DIR != DIR_R \
            or self.__DIR == DIR_U and DIR != DIR_D \
            or self.__DIR == DIR_D and DIR != DIR_U \
        :
            self.__DIR = DIR

    def move(self, foods):
        if not self.isalive(): return

        coords = self.__coords

        head = self.__coords[0]
        coords[1:] = coords[:-1]

        head += DELTA[self.__DIR]

        if head.x <= 0: head.x = N-2
        elif head.x >= N-1: head.x = 1

        if head.y <= 0: head.y = M-2
        elif head.y >= M-1: head.y = 1

        coords[0] = head

        for food in foods:
            if food.isalive() and food == head:
                self.inc()
                food.die()
                break

        if coords.count(head) > 1:
            self.die()

    def isalive(self):
        return self.__alive

    def die(self):
        self.__alive = False

    def inc(self):
        coords = self.__coords

        tail1 = coords[-1]
        tail2 = coords[-2]

        DIR = tail1.getdir(tail2)

        if DIR == None:
            DIR = DIR_R

        self += tail1 - DELTA[DIR]

    def gets(self, coord):
        try:
            i = self.__coords.index(coord)
            return SNAKE_HEAD[self.__DIR] if i == 0 else SNAKE_BODY[self.__DIR]
        except:
            pass
        