
from coord import Coord
from os import linesep as EOL

N = 20
M = 40

DIR_R = 'right'
DIR_L = 'left'
DIR_U = 'up'
DIR_D = 'down'

DIRS = [DIR_R, DIR_L, DIR_U, DIR_D]

DELTA = {
    DIR_R: Coord( 0,  1),
    DIR_L: Coord( 0, -1),
    DIR_U: Coord(-1,  0),
    DIR_D: Coord( 1,  0),
}

FOOD = chr(3)
EMPTY = chr(32)

WALL_H = chr(9552)
WALL_V = chr(9553)

WALL_1 = chr(9556)
WALL_3 = chr(9559)
WALL_2 = chr(9562)
WALL_4 = chr(9565)

KEY_ESC = 27

KEY_UP = 72
KEY_DOWN = 80
KEY_RIGHT = 77
KEY_LEFT = 75

KEY_W = 119
KEY_S = 115
KEY_D = 100
KEY_A = 97

SNAKE_HEAD = {
    DIR_R: chr(16),
    DIR_L: chr(17),
    DIR_U: chr(30),
    DIR_D: chr(31),
}

SNAKE_BODY = {
    DIR_R: chr(9632),
    DIR_L: chr(9632),
    DIR_U: chr(9632),
    DIR_D: chr(9632),
}
